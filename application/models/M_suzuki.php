<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_suzuki extends CI_Model {

    public function select($table){
    return $this->db->get($table);
  }
  public function selectlimit($table){
      $this->db->order_by('id_berita','DESC') ;
     $this->db->limit(6) ;
     return $this->db->get($table) ;
  }

  public function selectwhere($table,$data){
    return $this->db->get_where($table, $data);
  }
  public function selectwhereliimit($table,$data){
    $this->db->limit(4) ;
    return $this->db->get_where($table, $data);
  }

  function delete($where,$table){
    $this->db->where($where);
    $this->db->delete($table);
  }

  public function update($table,$data,$where){
    $this->db->update($table,$data,$where);
  }

  public function insert($table,$data){
    $this->db->insert($table,$data);
  }
  function cek_login($table,$where){
    return $this->db->get_where($table,$where);
  }

    function get_model(){
    $query = $this->db->query('SELECT * FROM automobil ORDER BY id_automobile ASC');
    return $query->result();
  }
          function getharga(){
          $this->db->select('harga.*, product.*');
          $this->db->from('harga');
          $this->db->join('product', 'harga.idProduct = product.idProduct');
          $data=$this->db->get();
          return $data;
        }

        function clientProduct4(){
          $this->db->select('mobil.*, automobil.*');
          $this->db->from('mobil');
          $this->db->join('automobil', 'mobil.id_automobile = automobil.id_automobile');
          $this->db->where('mobil.id_automobile', '4');
          $data=$this->db->get();
          return $data;
        }

        function get_mobilClient($id){
          $this->db->select('mobil.*');
          $this->db->where('id_mobil', $id);
          $data=$this->db->get();
          return $data;
        }
  //end of Product


}
