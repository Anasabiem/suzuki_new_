<?php $this->load->view('admin/side/head') ?>
<?php $this->load->view('admin/side/navbar') ?>
<br><br><hr>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Tambah Data</button>
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <form method="post" action="<?php echo base_url('admin/Hagra/tambah') ?>" enctype="multipart/form-data">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Modal Header</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <label for="exampleFormControlSelect2">Nama Product</label>
                    <select class="form-control" id="exampleFormControlSelect2" name="nmProduct">
                      <?php foreach ($p as $var): ?>
                      	<option value="<?php echo $var->idProduct ?>"><?php echo $var->nmProduct ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputUsername1">Tipe</label>
                    <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Username" name="tipe">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputUsername1">Harga</label>
                    <input type="number" class="form-control" id="exampleInputUsername1" placeholder="Username" name="harga">
                  </div>
                </div>
                <div class="modal-footer">
                  <button name="selesai" type="submit" class="btn btn-info btn-lg">Tambah</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Harga</h4>
      <p class="card-description">
        UMC  <code>Suzuki Jember</code>
      </p>
      <div class="table-responsive pt-3">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>
                No
              </th>
              <th>Nama Product</th>
              <th>Tipe</th>
              <th>Harga</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no = 1; foreach ($harga as $key): ?>
            <tr>
              <td>
                <?php echo $no++; ?>
              </td>
              <td>
                <?php echo $key->nmProduct ?>
              </td>
              <td>
                <?php echo $key->tipe ?>
              </td>
              <td>
                Rp.<?php echo number_format($key->harganya) ?>
              </td>
              <td>
                <a href="<?php echo base_url('admin/Hagra/hapus/'.$key-> idHarga) ?>" class="btn btn-danger waves-effect " title="Hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')"><i class="mdi mdi-delete"></i></a>
              </td>
            </tr>
          <?php endforeach ?>

        </tbody>
      </table>
      <br><br><hr><br>
      <br>
    </div>
  </div>
</div>
</div>
<?php $this->load->view('admin/side/js') ?>