 <?php $this->load->view("user/side/head"); ?>
 <?php $this->load->view("user/side/navbar"); ?>

 <div class="c-layout-page">
 	<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
 	<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold">
 		<div class="container">
 			<div class="c-page-title c-pull-left">
 				<h3 class="c-font-uppercase c-font-sbold">Contact Us</h3>
 				<!-- <h4 class="">Page Sub Title Goes Here</h4> -->
 			</div>
 			<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
 				<li><a href="#">Pages</a></li>
 				<li>/</li>
 				<li><a href="page-contact-1.html">Contact Us</a></li>

 			</ul>
 		</div>
 	</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
 	<!-- BEGIN: PAGE CONTENT -->
 	<!-- BEGIN: CONTENT/CONTACT/CONTACT-1 -->
 	<div class="c-content-box c-size-md c-bg-img-top c-no-padding c-pos-relative">
 		<div class="container">
 			<div class="c-content-contact-1 c-opt-1">
 				<div class="row" data-auto-height=".c-height">
 					<div class="col-sm-8 c-desktop"></div>
 					<div class="col-sm-4">
 						<div class="c-body">
 							<div class="c-section">
 								<h3>Suzuki Jember</h3>
 							</div>
 							<div class="c-section">
 								<div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Address</div>
 								<p>Jl. Hayam Wuruk Gg. 4, Gerdu, Sempusari, Kec. Kaliwates, Kabupaten Jember, Jawa Timur 68131</p>
 							</div>
 							<div class="c-section">
 								<div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Contacts</div>
 								<p><strong></strong> Anas <br/><strong></strong> +62 813-3354-9085</p>
 							</div>
 							<div class="c-section">
 								<div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Social</div><br/>
 								<ul class="c-content-iconlist-1 c-theme">
 									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
 									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
 									<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
 									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
 								</ul>
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div> 
 		<div id="gmapbg" class="c-content-contact-1-gmap" style="height: 615px; "><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126375.41533122341!2d113.61036254425186!3d-8.179395421126387!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd693ff9d1365b3%3A0xfc38e661b2e04bd9!2sPT.+UMC+Jember!5e0!3m2!1sid!2sid!4v1564545835211!5m2!1sid!2sid" width="100%" height="615" frameborder="0" style="border:0" allowfullscreen></iframe></div>
 	</div> <!-- END: CONTENT/CONTACT/CONTACT-1 -->
 	<!-- END: CONTENT/CONTACT/FEEDBACK-1 -->

 	<!-- END: PAGE CONTENT -->
 </div>
 <?php $this->load->view("user/side/footer"); ?>
 <?php $this->load->view("user/side/js"); ?>