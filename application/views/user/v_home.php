<!DOCTYPE html>
<html lang="en"  >
<head>
    <meta charset="utf-8"/>
    <title>Suzuki</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url()?>master/client/assets/plugins/socicon/socicon.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet" type="text/css"/>      
    <link href="<?php echo base_url()?>master/client/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/animate/animate.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/revo-slider/css/layers.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/revo-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/owl-carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/demos/default/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/demos/default/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/demos/default/css/themes/default.css" rel="stylesheet" id="style_theme" type="text/css"/>
    <link href="<?php echo base_url()?>master/client/assets/demos/default/css/custom.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-fullscreen"> 
    <?php $this->load->view('user/side/navbar') ?>
    <div class="c-layout-page">
        <section class="c-layout-revo-slider c-layout-revo-slider-13" dir="ltr">
            <div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile c-arrow-darken" data-bullets-pos="center">
                <div class="tp-banner rev_slider" data-version="5.0">
                    <ul>
                        <li data-index="rs-16" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="https://www.suzukimadiun.net/wp-content/uploads/2019/04/Gambar-Ertiga-Sport.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Let's go JANGO" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="https://www.suzukimadiun.net/wp-content/uploads/2019/04/Gambar-Ertiga-Sport.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-resizeme rs-parallaxlevel-0 c-font-white c-font-bold" 
                            id="slide-16-layer-1" 
                            data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                            data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                            data-fontsize="['70','70','70','45']"
                            data-lineheight="['70','70','70','50']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-transform_idle="o:1;"

                            data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                            data-start="1000" 
                            data-splitin="chars" 
                            data-splitout="none" 
                            data-responsive_offset="on" 

                            data-elementdelay="0.05" 

                            style="z-index: 5; white-space: nowrap;">AYO BELI MOBIL SUZUKI
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme rs-parallaxlevel-0 c-font-white" 
                        id="slide-16-layer-4" 
                        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                        data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" 
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-transform_idle="o:1;"

                        data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                        data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                        data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                        data-start="1500" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" 


                        style="z-index: 6; white-space: nowrap;">Di UMC Suzuki Jember 
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" 
                    id="slide-16-layer-8" 
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                    data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" 
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-style_hover="cursor:default;"

                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" 
                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                    data-start="2000" 
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on" 


                    style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i> 
                </div>
            </li>
            <!--END -->  
            <!-- BEGIN: SLIDE #2 -->

        </ul>
    </div>
</div>
</section><!-- END: LAYOUT/SLIDERS/REVO-SLIDER-13 -->

<!-- BEGIN: CONTENT/MISC/SERVICES-3 -->
<div class="c-content-box c-size-lg c-bg-grey-1">
    <div class="container">
        <div class="">
            <div class="row">
                <div class="col-md-7">
                    <div class="c-content-feature-5">
                        <div class="c-content-title-1 wow amimate fadeInDown">
                            <h3 class="c-left c-font-dark c-font-uppercase c-font-bold">Kenapa <br/>SUZUKI ?</h3>
                            <div class="c-line-left c-bg-blue-3 c-theme-bg"></div>
                        </div>
                        <div class="c-text wow animate fadeInLeft"> Produk ternama se Indonesia dengan kapasitas yang bagus dan nyaman. </div>
                        <img class="c-photo img-responsive wow animate fadeInUp" width="420" alt="" src="<?php echo base_url() ?>gambar/profil.jpeg" /> 
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="c-content-accordion-1 c-theme wow animate fadeInRight">
                        <div class="panel-group" id="accordion" role="tablist">
                            <div class="panel">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a class="c-font-bold c-font-uppercase c-font-19" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> Cara Membeli ? </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body c-font-18"> Lihat semua Produk kami. Pastikan sesuai dengan selera anda. Kemudian Hubungi kami <a href="https://api.whatsapp.com/send?phone=6281333549085">disini</a> </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed c-font-uppercase c-font-bold c-font-19" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Tentang  </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body c-font-18">  </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed c-font-uppercase c-font-bold c-font-19" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> _________ </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body c-font-18">  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END: CONTENT/FEATURES/FEATURES-15-1 -->

<!-- BEGIN: CONTENT/APPS/APP-2 -->
<!-- BEGIN: PARALLAX 1 - OPTION 4 -->
<div class="c-content-box c-size-md c-bg-parallax" style="background-image: url(<?php echo base_url()?>master/client/assets/base/img/content/backgrounds/bg-85.jpg)">
    <div class="container">
        <div class="c-content-app-1">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-font-uppercase c-center c-font-bold">Daftar Harga Mobil Suzuki</h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->
            <div class="c-diagram">
                <div class="c-lines-2" style="background-image: url(<?php echo base_url()?>master/client/assets/base/img/content/apps/app-line2.png)"></div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Product</th>
                            <th>Type</th>
                            <th>Harga</th>
                        </tr>
                    </thead>
                    <?php $no=1; foreach ($harga as $key): ?>
                    <tbody>
                        <tr>
                            <th><?php echo $no++; ?></th>
                            <th><?php echo $key->nmProduct ?></th>
                            <th><?php echo $key->tipe ?></th>
                            <th>Rp. <?php echo number_format($key->harganya) ?></th>
                        </tr>
                    </tbody>
                <?php endforeach ?>
            </table>
        </div>          
    </div>
</div> 
</div>
<!-- END: PARALLAX 1 - OPTION 4 -->
<!-- END: CONTENT/APPS/APP-2 -->

<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-3 -->
<div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url(<?php echo base_url()?>master/client/assets/base/img/content/backgrounds/bg-3.jpg)">
    <div class="container">
        <!-- Begin: testimonials 1 component -->
        <div class="c-content-testimonials-1" data-slider="owl">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-center c-font-white c-font-uppercase c-font-bold">Terbaru Dari Suzuki</h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->

            <!-- Begin: Owlcarousel -->
            <div class="c-bs-grid-small-space">
                <div class="row">
                    <?php foreach ($produk as $key): ?>
                        <div class="col-md-3 col-sm-6 c-margin-b-20">
                            <div class="c-content-product-2 c-bg-white c-border">
                                <div class="c-content-overlay">
                                    <?php if ($key->kategori=='promo'){ ?>
                                        <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Promo</div>
                                    <?php } elseif ($key->kategori=='baru') { ?>
                                        <div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
                                    <?php } elseif($key->kategori=='pb'){ ?>
                                        <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Promo</div>
                                        <div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
                                    <?php } else{ ?>
                                        
                                    <?php } ?>
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="<?php echo base_url('Product/detail/').$key->idProduct ?>" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                        </div>
                                    </div>
                                    <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(<?php echo $key->foto ?>);"></div>
                                </div>
                                <div class="c-info">
                                    <p class="c-title c-font-16 c-font-slim"><?php echo $key->nmProduct ?></p>
                                    <p class="c-price c-font-14 c-font-slim">Rp. <?php echo number_format($key->harga) ?> &nbsp;
                                        <!-- <span class="c-font-14 c-font-line-through c-font-red">$600</span> -->
                                    </p>
                                </div>
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group c-border-left c-border-top" role="group">
                                        <a href="https://api.whatsapp.com/send?phone=6281333549085" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Beli Sekarang</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>

                </div>


            </div>
            <!-- End-->
        </div>
        <!-- End-->
    </div>
</div><!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-3 -->

<!-- END: CONTENT/PRICING/PRICING-1 -->


<!-- END: PAGE CONTENT -->


</div>
<!-- END: PAGE CONTAINER -->

<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->
<a name="footer"></a>
<?php $this->load->view('user/side/footer') ?>
<!-- END: LAYOUT/FOOTERS/FOOTER-6 -->

<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div><!-- END: LAYOUT/FOOTERS/GO2TOP -->

<!-- BEGIN: LAYOUT/BASE/BOTTOM -->
<!-- BEGIN: CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url()?>master/client/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url()?>master/client/assets/plugins/jquery.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/jquery-migrate.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/jquery.easing.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/reveal-animate/wow.js" type="text/javascript" ></script>
<script src="<?php echo base_url()?>master/client/assets/demos/default/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript" ></script>

<!-- END: CORE PLUGINS -->

<!-- BEGIN: LAYOUT PLUGINS -->
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.video.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/typed/typed.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
<!-- END: LAYOUT PLUGINS -->

<!-- BEGIN: THEME SCRIPTS -->
<script src="<?php echo base_url()?>master/client/assets/base/js/components.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/base/js/components-shop.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/base/js/app.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {    
        App.init(); // init core    
    });
</script>
<!-- END: THEME SCRIPTS -->

<!-- BEGIN: PAGE SCRIPTS -->
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.kenburn.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>master/client/assets/demos/default/js/scripts/revo-slider/slider-13.js" type="text/javascript"></script>
<!-- END: PAGE SCRIPTS -->
<!-- END: LAYOUT/BASE/BOTTOM -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','<?php echo base_url()?>master/client/<?php echo base_url()?>master/client/../www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-64667612-1', 'themehats.com');
    ga('send', 'pageview');
</script>
</body>


<!-- Mirrored from themehats.com/themes/jango/demos/default/home-13.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 16 Sep 2018 12:21:54 GMT -->
</html>