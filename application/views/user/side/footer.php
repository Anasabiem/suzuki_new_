<footer class="c-layout-footer c-layout-footer-6 c-bg-grey-1">
            <div class="container">
                <div class="c-prefooter c-bg-white">
                    <div class="c-head">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="c-left">
                                    <div class="socicon">
                                        <a href="#" class="socicon-btn socicon-btn-circle socicon-solid c-bg-grey-1 c-font-grey-2 c-theme-on-hover socicon-facebook tooltips" data-original-title="Facebook" data-container="body"></a>
                                        <a href="#" class="socicon-btn socicon-btn-circle socicon-solid c-bg-grey-1 c-font-grey-2 c-theme-on-hover socicon-twitter tooltips" data-original-title="Twitter" data-container="body"></a>
                                        <a href="#" class="socicon-btn socicon-btn-circle socicon-solid c-bg-grey-1 c-font-grey-2 c-theme-on-hover socicon-youtube tooltips" data-original-title="Youtube" data-container="body"></a>
                                        <a href="#" class="socicon-btn socicon-btn-circle socicon-solid c-bg-grey-1 c-font-grey-2 c-theme-on-hover socicon-tumblr tooltips" data-original-title="Tumblr" data-container="body"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>
                    <div class="c-line"></div>
                    <div class="c-body">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <img height="300" width="300" src="<?php echo base_url() ?>gambar/profil.jpeg">
                            </div>
                            <br><hr>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="c-content-title-1 c-title-md">
                                    <h3 class="c-title c-font-uppercase c-font-bold">Contact Us</h3>
                                    <div class="c-line-left hide"></div>
                                </div>
                                <p class="c-address c-font-16"> Sales Executive
                                    <br/> Anas
                                    <br/> Whatsapp: <a href="https://api.whatsapp.com/send?phone=6281333549085">+62 813-3354-9085 </a>
                                    <br/> Email:
                                    <a href="mailto:">
                                        <span class="c-theme-color">Anasdarkness87@gmail.com</span>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="c-line"></div>
                    <div class="c-foot">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="c-content-title-1 c-title-md">
                                    <h3 class="c-font-uppercase c-font-bold">About
                                        <span class="c-theme-font">Suzuki</span>
                                    </h3>
                                    <div class="c-line-left hide"></div>
                                </div>
                                <p class="c-text c-font-16 c-font-regular">Suzuki Motor Corporation (スズキ株式会社 Suzuki Kabushiki-Kaisha) adalah perusahaan Jepang yang memproduksi kendaraan seperti mobil, mesin, ATV dan sepeda motor. Di Indonesia, PT. Suzuki Indomobil Sales adalah perusahaan yang memproduksi dan memasarkan mobil, ATV dan sepeda motor merek Suzuki. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-postfooter c-bg-dark-2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 c-col">
                            <p class="c-copyright c-font-grey">2019 &copy; kamata butra
                                <span class="c-font-grey-3">All Rights Reserved.</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>