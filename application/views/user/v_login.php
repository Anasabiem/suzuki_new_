<?php $this->load->view('admin/side/head') ?>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="main-panel">
        <div class="content-wrapper d-flex align-items-center auth px-0">
          <div class="row w-100 mx-0">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                <div class="brand-logo">
                  <img src="../../images/logo.svg" alt="logo">
                </div>
                <h4>Hello! let's get started</h4>
                <h6 class="font-weight-light">Sign in to continue.</h6>
                <form class="pt-3" action="<?php echo base_url('LoginAdmin/aksi_login') ?>" method="post">
                  <div class="form-group">
                    <input required="" type="text" class="form-control form-control-lg" name="username" placeholder="Username">
                  </div>
                  <div class="form-group">
                    <input type="password" required="" class="form-control form-control-lg" name="password" placeholder="Password">
                  </div>
                  <div class="mt-3">
                   <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit"><strong>Log In</strong></button>
                  </div>
                 <!--  <div class="my-2 d-flex justify-content-between align-items-center">
                    <div class="form-check">
                      <label class="form-check-label text-muted">
                        <input type="checkbox" class="form-check-input">
                        Keep me signed in
                      </label>
                    </div>
                    <a href="#" class="auth-link text-black">Forgot password?</a>
                  </div> -->
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <?php $this->load->view('admin/side/js') ?>