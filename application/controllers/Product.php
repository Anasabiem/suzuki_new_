<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	function index(){
		$data['produk'] = $this->M_suzuki->select('product')->result();
		$this->load->view('user/v_product',$data);
	}
	function detail(){
		$id = $this->uri->segment(3);
		$data['harga'] = $this->M_suzuki->selectwhere('harga',array('idProduct'=>$id));
		$data['detail']= $this->M_suzuki->selectwhere('product',array('idProduct'=>$id));
		// die(var_dump($id));
		$this->load->view('user/v_detail',$data);	
	}

}
