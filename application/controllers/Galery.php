<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galery extends CI_Controller {

	public function index()
	{
		$data['gambar'] = $this->M_suzuki->selectwhere('galeri',array('tipe'=>'biasa'));
		$this->load->view('user/v_galery',$data);
	}
}
