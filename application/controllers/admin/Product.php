<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function index()
	{
		if(!$this->session->userdata('status') == 'login'){
			redirect('LoginAdmin');
		}else{
			$data['gambar'] = $this->M_suzuki->select('product')->result();
			$this->load->view('admin/v_product',$data);
		}
	}

	public function tambah(){
		$url = $this->input->post('url');
		$nm = $this->input->post('nm_product');
		$kategori = $this->input->post('kategori');
		$detail = $this->input->post('details');
		$harga = $this->input->post('harga');
		$data = array(
			'nmProduct' => $nm,
			'detail' =>$detail,
			'kategori' => $kategori,
			'foto' => $url,
			'harga'=>$harga,
		);
		$this->db->insert('product',$data);
		redirect(base_url('admin/Product'));
	}

	public function update(){
		$id = $this->input->post('idP');
		$url = $this->input->post('url');
		$nm = $this->input->post('nm_product');
		$kategori = $this->input->post('kategori');
		$detail = $this->input->post('details');
		$harga = $this->input->post('harga');
		$data = array(
			'nmProduct' => $nm,
			'detail' =>$detail,
			'kategori' => $kategori,
			'foto' => $url,
			'harga'=> $harga
		);
		$this->db->update('product',$data,array('idProduct'=>$id));
		redirect(base_url('admin/Product'));
	}

	public function hapus($id){
		$where = array('idProduct'=>$id);
		$this -> M_suzuki -> delete($where,'product');
		header('location:'.base_url('admin/Product'));
	}
}