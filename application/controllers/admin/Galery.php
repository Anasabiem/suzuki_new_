<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galery extends CI_Controller {

	public function index()
	{
		if(!$this->session->userdata('status') == 'login'){
			redirect('LoginAdmin');
		}else{	
			$data['galeri']=$this->M_suzuki->selectwhere('galeri',array('tipe'=>'biasa'));
			$this->load->view('admin/v_galery',$data);
		}
	}
	public function create(){
		if(isset($_POST['btnSimpan'])){
			$config = array('upload_path' => './gallery/gambar_galery/',
				'allowed_types' => 'gif|jpg|png|jpeg'
			);
			$this -> load -> library ('upload',$config);
          // die(var_dump($rt));
			if ($this->upload->do_upload('pilih')){
				$upload_data = $this -> upload -> data ();
				$foto = "gallery/gambar_galery/".$upload_data['file_name'];
				$nm = $this->input->post('nmfoto');
				$data = array('foto' => $foto,
					'tipe'=>'biasa',
					'nama'=>$nm);
				$this->M_suzuki->insert('galeri',$data);
				redirect(base_url().'admin/Galery');
			}else{
				echo "ghghgh";
			}
		}
	}
	public function hapus($id){
		$where = array('idGaleri'=>$id);
		$this -> M_suzuki -> delete($where,'galeri');
		header('location:'.base_url('admin/Galery'));
	}
}
